import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputMessengerComponent } from './input-messenger.component';

describe('InputMessengerComponent', () => {
  let component: InputMessengerComponent;
  let fixture: ComponentFixture<InputMessengerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputMessengerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputMessengerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
