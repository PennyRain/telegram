import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderListChatComponent } from './header-list-chat.component';

describe('HeaderListChatComponent', () => {
  let component: HeaderListChatComponent;
  let fixture: ComponentFixture<HeaderListChatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderListChatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderListChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
