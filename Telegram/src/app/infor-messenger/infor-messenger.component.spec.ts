import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InforMessengerComponent } from './infor-messenger.component';

describe('InforMessengerComponent', () => {
  let component: InforMessengerComponent;
  let fixture: ComponentFixture<InforMessengerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InforMessengerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InforMessengerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
